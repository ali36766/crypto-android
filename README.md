# Sample Cyrpto Case Study:
This Application retrieves list of Crypto Currencies & Current Exchange Rates from Respective API's in user selected currency. Moreover, It also facilitates users with conversion.

# Architecture:
This project utilized MVI Architecure that has been used in conjuction with RX Java and several common Design Patterns
MVI was designed around the paradigm of reactive programming and uses observable flows to exchange messages between different entities. As a result, each of them will be independent and therefore more flexible and resilient.
In addition, information will always flow in one direction: this concept is known as Unidirectional Data Flow or UDF. Once the architecture is established, the developer will find it easier to reason and debug if necessary.

# Architecture Components:
MVI Architecture is built upon the following Components


1. Base View State
2. Base Partial Changes
3. Contract 
4. Presenter
5. Interactor 

# Patterns Used:
This Project has utilized the following patterns


1. Observable & Observer Pattern
2. Publish & Subscribe Pattern 
3. Builder Pattern 
4. Singleton Pattern
5. Repository Pattern

# How to Run:
This project has been fully configured to run seamlessly in Android Studio, However following things are mandatory

1. Android Studio Version should be Arctic Fox 2020.3.1 or Above
2. Use Internal Embedded JDK if prompted by Android Studio

To Run , Simply take pull from main branch and import project in android studio and choose build variant debug.

# API Keys 
The project uses an API Key from [Documentation](https://coinlayer.com/documentation) , In case API Key gets expired follow these steps
1. Goto [Account Creation](https://coinlayer.com) and Signup for a new account 
2. Then Goto [Dashboard](https://coinlayer.com/dashboard)  and Copy the generated API Access key
3. Open Project in Android Studio and add your API access in build.gradle(App Level) at line in debug & release buildTypes

            buildConfigField "String", "API_ACCESS_KEY", "\"YOUR_API_ACCESS_KEY\""


# Challenges:
No Major Challenges were faced during the development of this project
