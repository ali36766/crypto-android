package com.example.airlift_crypto.arch.view.launch

import com.example.airlift_crypto.arch.model.launch.GetCurrenciesInteractor
import com.example.airlift_crypto.arch.model.launch.LaunchViewState
import com.example.airlift_crypto.arch.model.launch.SaveCurrenciesInteractor
import io.reactivex.Observable

class LaunchPresenter(
    val getCurrenciesInteractor: LaunchContract.GetCurrenciesInteractor = GetCurrenciesInteractor(),
    val saveCurrenciesInteractor: LaunchContract.SaveCurrenciesInteractor,
    val getStoredCurrenciesInteractor: LaunchContract.GetStoredCurrencies
) : LaunchContract.Presenter() {
    override fun bindIntents() {

        val getCurrenciesIntent = intent { it.getCurrenciesIntent() }
            .switchMap {
                getCurrenciesInteractor.proceed()
            }

        val saveCurrenciesIntent = intent { it.saveCurrenciesIntent() }
            .switchMap { saveCurrenciesInteractor.proceed(it) }

        val getStoredCurrencies = intent { it.getStoredCurrenciesIntent() }
            .switchMap { getStoredCurrenciesInteractor.proceed() }

        val mergedIntents =
            Observable.merge(getCurrenciesIntent, saveCurrenciesIntent, getStoredCurrencies)

        subscribeViewState(
            mergedIntents.scan(LaunchViewState(), this::viewStateReducer),
            LaunchContract.View::render
        )
    }
}