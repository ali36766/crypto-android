package com.example.airlift_crypto.arch.view.conversion

import com.example.airlift_crypto.arch.model.conversion.ConversionViewState
import com.example.airlift_crypto.arch.model.conversion.GetConversionRateInteractor

class ConversionPresenter(
    var GetConversionInteractor: GetConversionRateInteractor = GetConversionRateInteractor()
) : ConversionContract.presenter() {
    override fun bindIntents() {
        val getConversionIntent = intent { it.getEstimatesIntent() }
            .switchMap { GetConversionInteractor.proceed(it.first, it.second, it.third) }

        subscribeViewState(
            getConversionIntent.scan(ConversionViewState(), this::viewStateReducer),
            ConversionContract.View::render
        )
    }
}