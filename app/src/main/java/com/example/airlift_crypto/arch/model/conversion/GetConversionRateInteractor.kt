package com.example.airlift_crypto.arch.model.conversion

import com.example.airlift_crypto.BuildConfig
import com.example.airlift_crypto.arch.intent.conversion.ConversionPartialChanges
import com.example.airlift_crypto.arch.view.conversion.ConversionContract
import com.example.airlift_crypto.arch.view.main.MainContract
import com.example.airlift_crypto.common.base.BasePartialChanges
import com.example.airlift_crypto.common.network.NetworkApiClient
import com.example.airlift_crypto.common.network.api.CryptoApi
import com.example.airlift_crypto.common.util.constants.Constants
import com.example.airlift_crypto.common.util.json.JsonUtils
import io.reactivex.Observable
import org.json.JSONObject

class GetConversionRateInteractor : ConversionContract.GetEstimateInteractor() {
    override fun proceed(from: String, to: String, amount: String): Observable<BasePartialChanges> {
        val response = NetworkApiClient
            .getApi(CryptoApi::class.java)
            .getConversionRate(BuildConfig.API_ACCESS_KEY, from, to, amount)

        return doProcessRemoteApiRequest(response)
    }

    override fun parseResponse(success: String): BasePartialChanges {
        val exchangeRate = JSONObject(success).getBoolean("success")

        return if (exchangeRate.toString() == "false")
            ConversionPartialChanges.ConvertedValueRetrieved(Constants.CONVERSION_FAILED, success)
        else
            ConversionPartialChanges.ConvertedValueRetrieved(Constants.CONVERSION_SUCCESS, JSONObject(success).getString("result"))
    }
}