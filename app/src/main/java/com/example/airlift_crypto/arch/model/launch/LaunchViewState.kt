package com.example.airlift_crypto.arch.model.launch

import com.example.airlift_crypto.common.base.BaseViewState
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto

class LaunchViewState(
    var currencyList: MutableList<CurrencyDetailDto> = mutableListOf()
) : BaseViewState() {
}