package com.example.airlift_crypto.arch.intent.conversion

import com.example.airlift_crypto.arch.model.conversion.ConversionViewState
import com.example.airlift_crypto.common.base.BasePartialChanges
import com.example.airlift_crypto.common.base.BaseViewState

sealed class ConversionPartialChanges: BasePartialChanges() {
    data class ConvertedValueRetrieved(val code:Int,val amount:String):ConversionPartialChanges(){
        override fun reduce(state: BaseViewState): BaseViewState {
            state as ConversionViewState
            state.amount = amount
            state.responseCode = code
            state.showError = false
            state.loading = false
            return state
        }

    }
}