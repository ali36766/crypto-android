package com.example.airlift_crypto.arch.intent.main

import com.example.airlift_crypto.arch.model.main.MainViewState
import com.example.airlift_crypto.common.base.BasePartialChanges
import com.example.airlift_crypto.common.base.BaseViewState
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.network.models.ExchangeRateDto

sealed class MainPartialChanges : BasePartialChanges() {
    data class ExchangeRateLoaded(val code: Int, val exchangeRate: MutableList<CurrencyDetailDto>) :
        BasePartialChanges() {
        override fun reduce(state: BaseViewState): BaseViewState {
            state as MainViewState
            state.exchangeRate = exchangeRate
            state.responseCode = code
            state.showError = false
            state.loading = false
            return state
        }
    }


}