package com.example.airlift_crypto.arch.model.launch

import com.example.airlift_crypto.arch.intent.launch.LaunchPartialChanges
import com.example.airlift_crypto.arch.view.launch.LaunchContract
import com.example.airlift_crypto.common.base.BasePartialChanges
import com.example.airlift_crypto.common.database.repository.CrypticRepo
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.util.constants.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetStoredCurrenciesInteractor(val repo: CrypticRepo) : LaunchContract.GetStoredCurrencies() {
    override fun proceed(): Observable<BasePartialChanges> {
        return repo.getCurrencies()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .map { map(it) }
            .cast(BasePartialChanges::class.java)
            .onErrorReturn {
                getErrorResponse(Constants.DEFAULT_ERROR_CODE, Constants.GENERIC_ERROR_MESSAGE)
            }
    }

    private fun map(list: List<CurrencyDetailDto>): BasePartialChanges {
        return if (list.isNullOrEmpty())
            LaunchPartialChanges.StoredCurrenciesLoaded(
                Constants.STORED_CURRENCIES_EMPTY,
                list as MutableList<CurrencyDetailDto>
            )
        else
            LaunchPartialChanges.StoredCurrenciesLoaded(
                Constants.STORED_CURRENCIES_LOADED,
                list as MutableList<CurrencyDetailDto>
            )
    }
}