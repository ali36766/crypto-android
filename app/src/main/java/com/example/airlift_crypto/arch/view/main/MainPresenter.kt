package com.example.airlift_crypto.arch.view.main

import com.example.airlift_crypto.arch.model.main.GetExchangeRateInteractor
import com.example.airlift_crypto.arch.model.main.MainViewState

class MainPresenter(
    val interactor: MainContract.GetExchangeRateInteractor = GetExchangeRateInteractor()
) : MainContract.Presenter() {
    override fun bindIntents() {
        val getExchangeRateIntent = intent { it.getExchangeRateIntent() }
            .switchMap { interactor.proceed(it.first,it.second) }

        subscribeViewState(
            getExchangeRateIntent.scan(MainViewState(), this::viewStateReducer),
            MainContract.View::render
        )
    }
}