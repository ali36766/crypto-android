package com.example.airlift_crypto.arch.view.conversion

import com.example.airlift_crypto.common.base.*
import io.reactivex.Observable

interface ConversionContract {
    interface View : BaseMvpView {
        fun getEstimatesIntent(): Observable<Triple<String, String, String>>
    }

    abstract class presenter : BasePresenter<View, BaseViewState>()

    abstract class GetEstimateInteractor : BaseInteractor() {
        abstract fun proceed(
            from: String,
            to: String,
            amount: String
        ): Observable<BasePartialChanges>
    }
}