package com.example.airlift_crypto.arch.view.launch

import com.example.airlift_crypto.common.base.*
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.network.models.ExchangeRateDto
import io.reactivex.Observable

interface LaunchContract {
    interface View : BaseMvpView {
        fun getCurrenciesIntent(): Observable<Boolean>
        fun saveCurrenciesIntent(): Observable<MutableList<CurrencyDetailDto>>
        fun getStoredCurrenciesIntent(): Observable<Boolean>
    }

    abstract class Presenter : BasePresenter<View, BaseViewState>()

    abstract class GetCurrenciesInteractor : BaseInteractor() {
        abstract fun proceed(): Observable<BasePartialChanges>
    }

    abstract class SaveCurrenciesInteractor : BaseInteractor() {
        abstract fun proceed(list: MutableList<CurrencyDetailDto>): Observable<BasePartialChanges>
    }

    abstract class GetStoredCurrencies : BaseInteractor() {
        abstract fun proceed(): Observable<BasePartialChanges>
    }
}