package com.example.airlift_crypto.arch.model.launch

import com.example.airlift_crypto.BuildConfig
import com.example.airlift_crypto.arch.intent.launch.LaunchPartialChanges
import com.example.airlift_crypto.arch.view.launch.LaunchContract
import com.example.airlift_crypto.common.base.BasePartialChanges
import com.example.airlift_crypto.common.database.repository.CrypticRepo
import com.example.airlift_crypto.common.network.NetworkApiClient
import com.example.airlift_crypto.common.network.api.CryptoApi
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.util.constants.Constants
import com.example.airlift_crypto.common.util.json.JsonUtils.parseJsonWithKeys
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable

class GetCurrenciesInteractor() : LaunchContract.GetCurrenciesInteractor() {
    override fun proceed(): Observable<BasePartialChanges> {

        val response = NetworkApiClient
            .getApi(CryptoApi::class.java)
            .getCurrencies(BuildConfig.API_ACCESS_KEY)

        return doProcessRemoteApiRequest(response)
    }

    override fun parseResponse(success: String): BasePartialChanges {
        val type = object : TypeToken<List<CurrencyDetailDto>>() {}.type
        val currencyList = GsonBuilder().create()
            .fromJson<ArrayList<CurrencyDetailDto>>(
                parseJsonWithKeys(success, "crypto").toString(),
                type
            )
        return LaunchPartialChanges.CurrenciesLoaded(
            Constants.CURRENCIES_LOADED,
            currencyList as MutableList<CurrencyDetailDto>
        )
    }

}