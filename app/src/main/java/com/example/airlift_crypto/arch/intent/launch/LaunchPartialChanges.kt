package com.example.airlift_crypto.arch.intent.launch

import com.example.airlift_crypto.arch.model.launch.LaunchViewState
import com.example.airlift_crypto.arch.model.main.MainViewState
import com.example.airlift_crypto.common.base.BasePartialChanges
import com.example.airlift_crypto.common.base.BaseViewState
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto

sealed class LaunchPartialChanges : BasePartialChanges() {
    data class CurrenciesLoaded(val code: Int, val currencyList: MutableList<CurrencyDetailDto>) :
        LaunchPartialChanges() {
        override fun reduce(state: BaseViewState): BaseViewState {
            state as LaunchViewState
            state.currencyList = currencyList
            state.responseCode = code
            state.loading = false
            state.showError = false
            return state
        }
    }

    data class StoredCurrenciesLoaded(
        val code: Int,
        val currencyList: MutableList<CurrencyDetailDto>
    ) : BasePartialChanges() {
        override fun reduce(state: BaseViewState): BaseViewState {
            state as LaunchViewState
            state.currencyList = currencyList
            state.responseCode = code
            state.showError = false
            state.loading = false
            return state
        }

    }

}