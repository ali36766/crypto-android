package com.example.airlift_crypto.arch.model.launch

import com.example.airlift_crypto.arch.intent.launch.LaunchPartialChanges
import com.example.airlift_crypto.arch.view.launch.LaunchContract
import com.example.airlift_crypto.common.base.BasePartialChanges
import com.example.airlift_crypto.common.database.repository.CrypticRepo
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.util.constants.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SaveCurrenciesInteractor(val repo: CrypticRepo) : LaunchContract.SaveCurrenciesInteractor() {
    override fun proceed(list: MutableList<CurrencyDetailDto>): Observable<BasePartialChanges> {
        return Observable.fromIterable(list)
            .flatMap { repo.saveCurrencies(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .cast(BasePartialChanges::class.java)
            .onErrorReturn {
                getErrorResponse(Constants.DEFAULT_ERROR_CODE, Constants.GENERIC_ERROR_MESSAGE)
            }
    }

}