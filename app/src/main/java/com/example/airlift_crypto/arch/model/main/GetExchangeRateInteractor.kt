package com.example.airlift_crypto.arch.model.main

import com.example.airlift_crypto.BuildConfig
import com.example.airlift_crypto.arch.intent.main.MainPartialChanges
import com.example.airlift_crypto.arch.view.main.MainContract
import com.example.airlift_crypto.common.base.BasePartialChanges
import com.example.airlift_crypto.common.network.NetworkApiClient
import com.example.airlift_crypto.common.network.api.CryptoApi
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.network.models.ExchangeRateDto
import com.example.airlift_crypto.common.util.constants.Constants
import com.example.airlift_crypto.common.util.json.JsonUtils.parseJsonWithOutKeys
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import org.json.JSONArray
import org.json.JSONObject

class GetExchangeRateInteractor : MainContract.GetExchangeRateInteractor() {
    private var availableCurrencyList = mutableListOf<CurrencyDetailDto>()
    override fun proceed(
        currencyList: MutableList<CurrencyDetailDto>,
        selectedCurrency: String
    ): Observable<BasePartialChanges> {
        this.availableCurrencyList = currencyList
        val response = NetworkApiClient
            .getApi(CryptoApi::class.java)
            .getExchangeRate(BuildConfig.API_ACCESS_KEY, selectedCurrency)
        return doProcessRemoteApiRequest(response)
    }

    override fun parseResponse(success: String): BasePartialChanges {
        val exchangeRate = parseJsonWithOutKeys(success, "rates")

        return MainPartialChanges.ExchangeRateLoaded(Constants.EXCHANGE_LOADED, mergeData(availableCurrencyList,exchangeRate))
    }

    private fun mergeData(currencyList: MutableList<CurrencyDetailDto>, exchangeRate: MutableList<ExchangeRateDto>): MutableList<CurrencyDetailDto>{
       for(i in 0 until exchangeRate.size){
           currencyList[i].exchangeRate = exchangeRate[i].rate
       }
        return currencyList
    }

}