package com.example.airlift_crypto.arch.model.conversion

import com.example.airlift_crypto.common.base.BaseViewState

class ConversionViewState(
    var amount: String = ""
) : BaseViewState() {
}