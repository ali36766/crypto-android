package com.example.airlift_crypto.arch.view.main

import com.example.airlift_crypto.common.base.*
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import io.reactivex.Observable

interface MainContract {
    interface View : BaseMvpView {
        fun getExchangeRateIntent(): Observable<Pair<MutableList<CurrencyDetailDto>,String>>
    }

    abstract class Presenter : BasePresenter<View, BaseViewState>()

    abstract class GetExchangeRateInteractor : BaseInteractor() {
        abstract fun proceed(currencyList: MutableList<CurrencyDetailDto>,selectedCurrency:String): Observable<BasePartialChanges>
    }

    abstract class GetConversionRateInteractor : BaseInteractor() {
        abstract fun proceed(
            from: String,
            to: String,
            value: String
        ): Observable<BasePartialChanges>
    }
}