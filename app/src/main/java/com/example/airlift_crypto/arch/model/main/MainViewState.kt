package com.example.airlift_crypto.arch.model.main

import com.example.airlift_crypto.common.base.BaseViewState
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.network.models.ExchangeRateDto

class MainViewState(
    var exchangeRate: MutableList<CurrencyDetailDto> = mutableListOf()
) : BaseViewState() {

}