package com.example.airlift_crypto.common.base

import android.util.Log
import com.example.airlift_crypto.common.util.constants.Constants
import com.example.airlift_crypto.common.util.json.JsonUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.IOException

const val TAG = "BaseInteractor"

abstract class BaseInteractor(private val errorCod: Int = Constants.DEFAULT_ERROR_CODE) {


    fun doProcessRemoteApiRequest(observable: Observable<Response<ResponseBody>>): Observable<BasePartialChanges> {
        return observable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .map { getResponseIntent(it) }
            .cast(BasePartialChanges::class.java)
            .onErrorReturn { throwable ->
                if (throwable is IOException)
                    getErrorResponse(code = Constants.INTERNET_CONNECTION_ERROR, throwable.message)
                else
                    getErrorResponse(Constants.DEFAULT_ERROR_CODE, throwable.message)
            }
            .startWith(BasePartialChanges.Loading)
    }


    private fun getResponseIntent(

        res: Response<ResponseBody>,
        code: Int = res.code()
    ): BasePartialChanges {
        return when (code) {
            200 -> getSuccessResponse(res.body()?.string())
            else -> getErrorResponse(errorCod, res.errorBody()?.string())
        }
    }

    protected open fun getErrorResponse(
        code: Int,
        error: String?
    ): BasePartialChanges {

        if (error != null) {
            Log.d(TAG, error)
        }
        return BasePartialChanges.Error(Constants.GENERIC_ERROR_MESSAGE, code)

    }

    protected open fun getSuccessResponse(success: String?): BasePartialChanges {
        if (success.isNullOrEmpty() || !JsonUtils.validateJSONString(success)) {
            return BasePartialChanges.Error(code = Constants.DEFAULT_ERROR_CODE)
        }
        return parseResponse(success)
    }

    protected open fun parseResponse(success: String): BasePartialChanges {
        return BasePartialChanges.Success(code = Constants.RESPONSE_OK)
    }


}