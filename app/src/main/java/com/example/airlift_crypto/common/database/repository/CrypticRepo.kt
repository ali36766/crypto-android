package com.example.airlift_crypto.common.database.repository

import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import io.reactivex.Completable
import io.reactivex.Observable

interface CrypticRepo {

    fun saveCurrencies(currency: CurrencyDetailDto): Observable<Completable>
    fun getCurrencies(): Observable<List<CurrencyDetailDto>>
}