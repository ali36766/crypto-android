package com.example.airlift_crypto.common.base

import com.hannesdorfmann.mosby3.mvp.MvpView

interface BaseMvpView : MvpView {
    fun render(viewState: BaseViewState)
}