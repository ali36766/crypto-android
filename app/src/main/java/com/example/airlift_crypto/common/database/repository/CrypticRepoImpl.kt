package com.example.airlift_crypto.common.database.repository

import com.example.airlift_crypto.common.database.dao.CurrencyDao
import com.example.airlift_crypto.common.database.dto.DbCurrencies
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import io.reactivex.Completable
import io.reactivex.Observable

class CrypticRepoImpl(private val currencyDao: CurrencyDao) : CrypticRepo {
    override fun saveCurrencies(currency: CurrencyDetailDto): Observable<Completable> {
        return currencyDao.executeSaveCurrenciesTransaction(
            DbCurrencies(
                symbol = currency.symbol,
                name = currency.name, iconUrl = currency.icon_url
            )
        ).toObservable()
    }


    override fun getCurrencies(): Observable<List<CurrencyDetailDto>> {
        return currencyDao.executeGetCurrencyTransaction().map {
            val list = it
            list
        }
    }
}