package com.example.airlift_crypto.common.util.json

import com.example.airlift_crypto.common.network.models.ExchangeRateDto
import org.json.JSONArray
import org.json.JSONObject

object JsonUtils {

    fun createJSONObject(input: String): JSONObject {
        return JSONObject(input)
    }

    fun getJSONObject(jsonString: String, key: String): JSONObject {
        return createJSONObject(jsonString).getJSONObject(key)
    }

    @JvmStatic
    fun validateJSONString(string: String?): Boolean {
        return try {
            if (string != null) {
                JSONObject(string)
                true
            } else
                false
        } catch (e: Exception) {
            try {
                JSONArray(string)
                true
            } catch (e: Exception) {
                false
            }

        }
    }

    @JvmStatic
    fun parseJsonWithKeys(success: String, key: String): JSONArray {
        val checkJson = JSONObject(success).getJSONObject(key)
        val iterator = checkJson.keys()
        val jsonArray = JSONArray()
        while (iterator.hasNext()) {
            val jsonKey = iterator.next()
            jsonArray.put(checkJson.get(jsonKey))
        }
        return jsonArray

    }

    @JvmStatic
    fun parseJsonWithOutKeys(success: String, key: String): MutableList<ExchangeRateDto> {
        val checkJson = JSONObject(success).getJSONObject(key)
        val iterator = checkJson.keys()
        val list = mutableListOf<ExchangeRateDto>()
        while (iterator.hasNext()) {
            val jsonKey = iterator.next()
            list.add(ExchangeRateDto(symbol = jsonKey, rate = checkJson.get(jsonKey).toString()))
        }
        return list
    }
}