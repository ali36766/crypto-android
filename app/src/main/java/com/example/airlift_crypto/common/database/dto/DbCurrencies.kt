package com.example.airlift_crypto.common.database.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "currency",primaryKeys = (["name"]))
data class DbCurrencies(
    @ColumnInfo(index = true)
    val symbol: String,
    val name: String,
    val iconUrl: String
) {
}