package com.example.airlift_crypto.common.network.api

import com.example.airlift_crypto.BuildConfig
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CryptoApi {

    @GET("list")
    fun getCurrencies(
        @Query("access_key") accessKey: String
    ): Observable<Response<ResponseBody>>

    @GET("live")
    fun getExchangeRate(
        @Query("access_key") accessKey: String,
        @Query("target") targetCurrency: String
    ): Observable<Response<ResponseBody>>


    @GET("convert")
    fun getConversionRate(
        @Query("access_key") accessKey: String,
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("amount") amount: String

    ): Observable<Response<ResponseBody>>
}