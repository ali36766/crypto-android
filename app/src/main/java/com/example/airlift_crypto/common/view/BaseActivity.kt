package com.example.airlift_crypto.common.view

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

const val TAG = "BaseActivity"

open class BaseActivity : AppCompatActivity() {

    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "onCreate() is called in ${this.javaClass.simpleName}")
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume() is called in ${this.javaClass.simpleName}")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "onPause() is called in ${this.javaClass.simpleName}")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy() is called in ${this.javaClass.simpleName}")
    }

    override fun onBackPressed() {
        Log.i(TAG, "onBackPressed() is called in ${this.javaClass.simpleName}")
        super.onBackPressed()
    }

}