package com.example.airlift_crypto.common.network.models

import java.io.Serializable

data class ExchangeRateDto(
    var symbol: String,
    var rate: String,
    var name: String? = null,
    var icon_url: String? = null
) : Serializable {
}