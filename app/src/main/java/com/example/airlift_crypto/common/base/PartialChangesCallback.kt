package com.example.airlift_crypto.common.base

interface PartialChangesCallback {

    fun reduce(state: BaseViewState): BaseViewState
}