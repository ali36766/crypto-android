package com.example.airlift_crypto.common.base

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter


abstract class BasePresenter<V : BaseMvpView, S : BaseViewState> : MviBasePresenter<V, S>() {
    fun viewStateReducer(
        previousState: BaseViewState,
        partialChanges: BasePartialChanges
    ) = partialChanges.reduce(previousState)
}