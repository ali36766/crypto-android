package com.example.airlift_crypto.common.util.database

import android.annotation.SuppressLint
import android.database.Cursor

object DatabaseUtils {

    @SuppressLint("Range")
    fun getStringFromCursor(cursor: Cursor, index: String): String {
        return cursor.getString(cursor.getColumnIndex(index)).orEmpty()
    }
}