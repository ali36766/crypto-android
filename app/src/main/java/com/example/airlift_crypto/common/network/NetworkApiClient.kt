package com.example.airlift_crypto.common.network

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.IllegalStateException
import java.util.concurrent.TimeUnit

    class NetworkApiClient private constructor() {

        class ApiClientConfiguration(var baseUrl: HttpUrl = "http://localhost/".toHttpUrl()) {
            private val builder = OkHttpClient.Builder()

            fun addTimeOutInMinutes(timeoutInterval: Long, timeUnit: TimeUnit) {
                builder.writeTimeout(timeoutInterval, timeUnit)
                builder.readTimeout(timeoutInterval, timeUnit)
            }

            fun getOkHttpClient(): OkHttpClient {
                return builder.build()
            }

            fun getDefaultConverter(): GsonConverterFactory {
                return GsonConverterFactory.create()
            }

            fun getRxJavaCallAdapterFactory(): RxJava2CallAdapterFactory {
                return RxJava2CallAdapterFactory.create()
            }

        }

        companion object {
            private var instance: Retrofit? = null

            private fun initClientInternal(apiClientConfiguration: ApiClientConfiguration): Retrofit {
                return Retrofit.Builder()
                    .baseUrl(apiClientConfiguration.baseUrl)
                    .addCallAdapterFactory(apiClientConfiguration.getRxJavaCallAdapterFactory())
                    .addConverterFactory(apiClientConfiguration.getDefaultConverter())
                    .client(apiClientConfiguration.getOkHttpClient())
                    .build()

            }

            @JvmStatic
            fun initializeNetworkClient(apiClientConfiguration: ApiClientConfiguration) {
                synchronized(NetworkApiClient::class.java) {
                    if (instance == null)
                        instance = initClientInternal(apiClientConfiguration)
                    else
                        throw IllegalStateException("Singleton Instance already exists.")
                }
            }

            @JvmStatic
            fun getApiClient(): Retrofit {
                return instance!!
            }

            @JvmStatic
            fun <T> getApi(apiClass: Class<T>): T {
                return getApiClient().create(apiClass)
            }

            @JvmStatic
            fun dispose() {
                instance = null
                System.gc()
                System.runFinalization()
            }
        }
    }