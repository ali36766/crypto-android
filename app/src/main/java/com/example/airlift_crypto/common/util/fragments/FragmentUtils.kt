package com.example.airlift_crypto.common.util.fragments

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

class FragmentUtils(activity: AppCompatActivity) {
    private var fragmentManager: FragmentManager? = null
    private var activityReference: AppCompatActivity? = null

    init {
        activityReference = activity
        fragmentManager = activityReference!!.supportFragmentManager
    }

    fun openfragment(fragment: Fragment, container: Int) {

        val transaction = fragmentManager!!.beginTransaction()
        transaction.replace(container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()

    }
}