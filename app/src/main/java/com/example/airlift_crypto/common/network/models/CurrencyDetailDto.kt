package com.example.airlift_crypto.common.network.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CurrencyDetailDto(

    @SerializedName("symbol")
    var symbol: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("icon_url")
    var icon_url: String = "",
    var exchangeRate: String = ""
) : Serializable {
}