package com.example.airlift_crypto.common.util.constants


object Constants {

    //ERROR CODES
    const val DEFAULT_ERROR_CODE = 1
    const val INTERNET_CONNECTION_ERROR = 2
    const val CONVERSION_FAILED = 3


    //ERROR MESSAGES
    const val GENERIC_ERROR_MESSAGE = "We are unable to process your request. Please try again."

    //SUCCESS CODE
    const val RESPONSE_OK = 200
    const val CURRENCIES_LOADED = 201
    const val EXCHANGE_LOADED = 202
    const val STORED_CURRENCIES_LOADED = 700
    const val STORED_CURRENCIES_EMPTY = 701
    const val CONVERSION_SUCCESS= 203

}