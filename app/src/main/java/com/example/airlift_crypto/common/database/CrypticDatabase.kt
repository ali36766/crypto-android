package com.example.airlift_crypto.common.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.airlift_crypto.common.database.dao.CurrencyDao
import com.example.airlift_crypto.common.database.dto.DbCurrencies

@Database
    (
    version = 1, entities = [
    DbCurrencies::class
]
)
abstract class CrypticDatabase : RoomDatabase() {

    abstract fun getCurrencyDao(): CurrencyDao
    companion object {
        private val sLock = Any()
        private var INSTANCE: CrypticDatabase? = null

        @JvmStatic
        fun getInstance(context: Context): CrypticDatabase {
            synchronized(sLock)
            {
                if (INSTANCE == null) {
                    INSTANCE =
                        Room.databaseBuilder(context, CrypticDatabase::class.java, "db_cryptic")
                            .build()
                }
            }
            return INSTANCE!!
        }
    }
}