package com.example.airlift_crypto.common.database.dao

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.ABORT
import androidx.room.Query
import androidx.room.Transaction
import com.example.airlift_crypto.common.database.dto.DbCurrencies
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.util.database.DatabaseUtils.getStringFromCursor
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
abstract class CurrencyDao {

    @Insert(onConflict = ABORT)
    abstract fun saveCurrencies(dbCurrencies: DbCurrencies)

    @Transaction
    @Query(
        """
        SELECT * FROM currency
    """
    )
    abstract fun getCurrencies(): Cursor


    fun executeSaveCurrenciesTransaction(dbCurrencies: DbCurrencies): Completable {
        return Completable.fromAction {
            saveCurrencies(dbCurrencies)
        }
    }

    fun executeGetCurrencyTransaction(): Observable<List<CurrencyDetailDto>> {
        return Observable.fromCallable {
            getCurrencies()
        }
            .map { cursor ->
                val currencies = mutableListOf<CurrencyDetailDto>()
                while (cursor.moveToNext()) {

                    val currency = CurrencyDetailDto(
                        symbol = getStringFromCursor(cursor, "symbol"),
                        name = getStringFromCursor(cursor, "name"),
                        icon_url = getStringFromCursor(cursor, "iconUrl")
                    )
                    currencies.add(currency)
                }
                return@map currencies
            }
    }

}