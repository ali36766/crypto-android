package com.example.airlift_crypto.common.util.glide

import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.airlift_crypto.R

object GlideUtils {

    fun loadImage(imageUrl: String, view: AppCompatImageView) {
        val requestOptions = RequestOptions()
            .centerCrop()
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)

        Glide.with(view.context)
            .load(imageUrl)
            .thumbnail(0.3f)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .apply(requestOptions)
            .into(view)
    }

    fun loadImageDrawable(imageView: AppCompatImageView, imageDrawable: Int) {
        Glide.with(imageView.context)
            .load(imageDrawable)
            .into(imageView)
    }
}