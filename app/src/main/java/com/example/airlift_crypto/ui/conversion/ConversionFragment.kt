package com.example.airlift_crypto.ui.conversion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.airlift_crypto.DataBinderMapperImpl
import com.example.airlift_crypto.R
import com.example.airlift_crypto.arch.model.conversion.ConversionViewState
import com.example.airlift_crypto.arch.view.conversion.ConversionContract
import com.example.airlift_crypto.arch.view.conversion.ConversionPresenter
import com.example.airlift_crypto.common.base.BaseViewState
import com.example.airlift_crypto.common.util.constants.Constants
import com.example.airlift_crypto.databinding.FragmentConversionBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


class ConversionFragment : Fragment(), ConversionContract.View, View.OnClickListener {
    private lateinit var binding: FragmentConversionBinding
    private val getConversionSubject = PublishSubject.create<Triple<String, String, String>>()

    private val presenter: ConversionContract.presenter by lazy {
        ConversionPresenter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_conversion, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        init()
    }

    private fun init() {

        listeners()
    }

    private fun listeners() {
        binding.btEstimate.setOnClickListener(this)
    }

    override fun getEstimatesIntent(): Observable<Triple<String, String, String>> {
        return getConversionSubject
            .map { Triple(it.first, it.second, it.third) }
    }

    override fun render(viewState: BaseViewState) {
        if (viewState.responseCode == Constants.CONVERSION_SUCCESS) {
            val message = (viewState as ConversionViewState).amount
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        }
        if (viewState.responseCode == Constants.CONVERSION_FAILED) {
            val message = (viewState as ConversionViewState).amount
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        }
        if (viewState.responseCode == Constants.INTERNET_CONNECTION_ERROR) {
            Toast.makeText(requireContext(), "Check your Internet Connection!", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.bt_estimate -> {
                if (binding.etFrom.text.toString().isBlank()) {
                    binding.from.error = "Missing"
                }

                if (binding.etTo.text.toString().isBlank()) {
                    binding.to.error = "Missing"

                }
                if (binding.etFromAmount.text.toString().isBlank()) {
                    binding.fromAmount.error = "Missing"
                } else {
                    getConversionSubject.onNext(
                        Triple(
                            binding.etFrom.text.toString(),
                            binding.etTo.text.toString(),
                            binding.etFromAmount.text.toString()
                        )
                    )
                }
            }
        }


    }
}