package com.example.airlift_crypto.ui.market

import android.annotation.SuppressLint
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.SyncStateContract
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.airlift_crypto.R
import com.example.airlift_crypto.arch.model.main.MainViewState
import com.example.airlift_crypto.arch.view.main.MainContract
import com.example.airlift_crypto.arch.view.main.MainPresenter
import com.example.airlift_crypto.common.adapters.ListingAdaptor
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.databinding.FragmentMarketBinding
import com.example.airlift_crypto.common.adapters.ItemLayoutManger
import com.example.airlift_crypto.common.base.BaseViewState
import com.example.airlift_crypto.common.util.constants.Constants
import com.example.airlift_crypto.databinding.LayoutCurrencyListBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.io.Serializable

class MarketFragment : Fragment(), AdapterView.OnItemSelectedListener, ItemLayoutManger,
    MainContract.View {
    private lateinit var binding: FragmentMarketBinding
    private var spinner: Spinner? = null
    private var selectedCurrency: String = ""
    private var getExchangeRateSubject =
        PublishSubject.create<Pair<MutableList<CurrencyDetailDto>, String>>()
    private lateinit var currencyList: MutableList<CurrencyDetailDto>
    private val market: ListingAdaptor<CurrencyDetailDto> by lazy {
        ListingAdaptor<CurrencyDetailDto>(requireContext(), this)
    }

    private val presenter: MainContract.Presenter by lazy {
        MainPresenter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            currencyList =
                (it.getSerializable(KEY_CURRENCY_DETAILS) as MutableList<CurrencyDetailDto>?)!!

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_market, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        presenter.attachView(this)
        loadSpinner()
        attachSpinnerListener()
        market.bindRecyclerView(binding.rvList)
    }

    private fun loadSpinner() {
        spinner = binding.spinner
        val staticAdapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.currencies,
            android.R.layout.simple_spinner_item
        )
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.adapter = staticAdapter
    }

    override fun bindView(view: View, getAdapterPosition: () -> Int) {
       val rowBinding = DataBindingUtil.bind<LayoutCurrencyListBinding>(view)
        rowBinding?.currency?.text = selectedCurrency
    }

    private fun attachSpinnerListener() {
        spinner!!.onItemSelectedListener = this
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (p0 != null) {
            selectedCurrency = p0.getItemAtPosition(p2).toString()
            getExchangeRateSubject.onNext(Pair(currencyList, selectedCurrency))
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    companion object {
        const val KEY_CURRENCY_DETAILS = "keycontactdetails"

        @JvmStatic
        fun newInstance(list: Serializable) =
            MarketFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(KEY_CURRENCY_DETAILS, list)
                }
            }
    }

    override fun getLayoutId(position: Int): Int {
        return R.layout.layout_currency_list
    }

    override fun getExchangeRateIntent(): Observable<Pair<MutableList<CurrencyDetailDto>, String>> {
        return getExchangeRateSubject
            .map { Pair(it.first, it.second) }
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun render(viewState: BaseViewState) {
        if (viewState.responseCode == Constants.EXCHANGE_LOADED) {
            val exchangeList = (viewState as MainViewState).exchangeRate
            market.notifyDataSetChanged()
            market.replaceItems(exchangeList as ArrayList<CurrencyDetailDto>)


        }
        if (viewState.responseCode == Constants.INTERNET_CONNECTION_ERROR) {
            Toast.makeText(requireContext(), Constants.GENERIC_ERROR_MESSAGE, Toast.LENGTH_SHORT)
                .show()
            val list = mutableListOf<CurrencyDetailDto>()
            market.addItems(list as ArrayList<CurrencyDetailDto>)

        }

    }
}
