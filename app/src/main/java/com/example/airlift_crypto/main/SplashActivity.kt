package com.example.airlift_crypto.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.airlift_crypto.arch.model.launch.GetCurrenciesInteractor
import com.example.airlift_crypto.arch.model.launch.GetStoredCurrenciesInteractor
import com.example.airlift_crypto.arch.model.launch.LaunchViewState
import com.example.airlift_crypto.arch.model.launch.SaveCurrenciesInteractor
import com.example.airlift_crypto.arch.view.launch.LaunchContract
import com.example.airlift_crypto.arch.view.launch.LaunchPresenter
import com.example.airlift_crypto.common.base.BaseViewState
import com.example.airlift_crypto.common.database.CrypticDatabase
import com.example.airlift_crypto.common.database.repository.CrypticRepo
import com.example.airlift_crypto.common.database.repository.CrypticRepoImpl
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.view.BaseActivity
import com.example.airlift_crypto.databinding.ActivitySplashBinding
import com.example.airlift_crypto.common.util.constants.Constants
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.*
import java.io.Serializable

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity(), LaunchContract.View, CoroutineScope by MainScope() {

    private lateinit var binding: ActivitySplashBinding
    private var getCurrencySubject = PublishSubject.create<Boolean>()
    private var getStoredCurrencySubject = PublishSubject.create<Boolean>()
    private var saveCurrencySubject = PublishSubject.create<MutableList<CurrencyDetailDto>>()
    private var currencyList = mutableListOf<CurrencyDetailDto>()

    private val presenter: LaunchContract.Presenter by lazy {
        val database = CrypticDatabase.getInstance(this)
        val dao = database.getCurrencyDao()
        val saveInteractor = SaveCurrenciesInteractor(CrypticRepoImpl(dao))
        val getStoredInteractor = GetStoredCurrenciesInteractor(CrypticRepoImpl(dao))
        LaunchPresenter(
            saveCurrenciesInteractor = saveInteractor,
            getStoredCurrenciesInteractor = getStoredInteractor
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        presenter.attachView(this)
        getStoredCurrencySubject.onNext(true)
    }

    override fun getCurrenciesIntent(): Observable<Boolean> {
        return getCurrencySubject
    }

    override fun saveCurrenciesIntent(): Observable<MutableList<CurrencyDetailDto>> {
        return saveCurrencySubject
            .map { it }
    }

    override fun getStoredCurrenciesIntent(): Observable<Boolean> {
        return getStoredCurrencySubject
    }

    override fun render(viewState: BaseViewState) {
        if (viewState.responseCode == Constants.CURRENCIES_LOADED) {
            currencyList = (viewState as LaunchViewState).currencyList
            saveCurrencySubject.onNext(currencyList)
            launchActivity(MainActivity())

        }
        if (viewState.responseCode == Constants.STORED_CURRENCIES_LOADED) {
            currencyList = (viewState as LaunchViewState).currencyList
            launchActivity(MainActivity())
        }
        if (viewState.responseCode == Constants.STORED_CURRENCIES_EMPTY) {
            getCurrencySubject.onNext(true)
        }

        if (viewState.responseCode == Constants.INTERNET_CONNECTION_ERROR) {
            Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    private fun launchActivity(activity: AppCompatActivity) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("Currencies", currencyList as Serializable)
        startActivity(intent)
    }

    override fun onBackPressed() {

    }
}