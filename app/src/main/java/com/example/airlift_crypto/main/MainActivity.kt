package com.example.airlift_crypto.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.example.airlift_crypto.R
import com.example.airlift_crypto.arch.model.main.MainViewState
import com.example.airlift_crypto.arch.view.main.MainContract
import com.example.airlift_crypto.arch.view.main.MainPresenter
import com.example.airlift_crypto.common.base.BaseViewState
import com.example.airlift_crypto.common.network.models.CurrencyDetailDto
import com.example.airlift_crypto.common.network.models.ExchangeRateDto
import com.example.airlift_crypto.common.util.constants.Constants
import com.example.airlift_crypto.common.util.fragments.FragmentUtils
import com.example.airlift_crypto.common.util.glide.GlideUtils
import com.example.airlift_crypto.common.view.BaseActivity
import com.example.airlift_crypto.databinding.ActivityMainBinding
import com.example.airlift_crypto.ui.conversion.ConversionFragment
import com.example.airlift_crypto.ui.market.MarketFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.*
import java.io.Serializable

class MainActivity : BaseActivity(), NavigationBarView.OnItemSelectedListener {
    private lateinit var binding: ActivityMainBinding
    private var currencyList = mutableListOf<CurrencyDetailDto>()
    private var getExchangeRateSubject = PublishSubject.create<String>()
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var fragmentUtils: FragmentUtils
    private val presenter: MainContract.Presenter by lazy {
        MainPresenter()
    }
    private var exchangeList = mutableListOf<ExchangeRateDto>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        listeners()
    }

    private fun init() {
        runBlocking {
            fetchCurrencies()
        }
        fragmentUtils = FragmentUtils(this)
        bottomNavigation = binding.navigationView
        fragmentUtils.openfragment(
            MarketFragment.newInstance(currencyList as Serializable),
            R.id.container
        )

    }

    private fun listeners() {
        binding.navigationView.setOnItemSelectedListener(this)
    }

    private suspend fun fetchCurrencies() {
        withContext(Dispatchers.IO) {
            currencyList =
                intent.getSerializableExtra("Currencies") as MutableList<CurrencyDetailDto>
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_market -> {
                fragmentUtils.openfragment(
                    MarketFragment.newInstance(currencyList as Serializable),
                    R.id.container
                )
                return true
            }
            R.id.navigation_convert -> {
                fragmentUtils.openfragment(ConversionFragment(), R.id.container)
                return true
            }
            else -> {
                return false
            }
        }
    }

    override fun onBackPressed() {

    }

}

@BindingAdapter("imageUrl")
fun setImageUrl(imgView: AppCompatImageView, imageUrl: String) {
    imgView.setImageBitmap(null)

    if (imageUrl.isNotEmpty()) {
        GlideUtils.loadImage(imageUrl, imgView)
    }
}