package com.example.airlift_crypto.main

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.room.Room
import com.example.airlift_crypto.BuildConfig
import com.example.airlift_crypto.common.database.CrypticDatabase
import com.example.airlift_crypto.common.network.NetworkApiClient
import okhttp3.HttpUrl.Companion.toHttpUrl
import java.util.concurrent.TimeUnit

class CoreApplication : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var context: Context
        var database: CrypticDatabase? = null

        @JvmStatic
        fun getAppContext(): Context {
            return context
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        initializeNetworkApiClient()

    }


    private fun initializeNetworkApiClient() {

        val apiClientConfiguration =
            NetworkApiClient.ApiClientConfiguration(BuildConfig.API_SERVER.toHttpUrl()).apply {
                addTimeOutInMinutes(2, TimeUnit.MINUTES)
            }

        NetworkApiClient.initializeNetworkClient(apiClientConfiguration)
    }


}